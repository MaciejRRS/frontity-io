import React, { useState, useEffect } from 'react'
import NavLink from "@frontity/components/link"
import { styled } from "frontity"
import { Flex, Link } from '../../../common/styles'
import Arrow from '../../../common/sprites/arrow.svg'

// icons desctop
import Arrows from '../../../common/sprites/coursor.svg'
import Helpwise from '../../../common/sprites/help_wise.svg'
import JustCall from '../../../common/sprites/just_call.svg'
import JustCallIQ from '../../../common/sprites/just_call_iq.svg'

// icons mobile
import JustCallMobile from '../../../common/sprites/JUSTCALL-mobile.png'
import JustCallIQMobile from '../../../common/sprites/iqMobile.png'
import HelpWiseMobile from '../../../common/sprites/helpwiseMobile.png'



import useGaTracker from '../../../common/tracking/Tracking'
import { connect } from "frontity"

const HeaderContainer = styled.header`
    position: fixed;
    top: 40px;
    left: 0;
    right: 0;
    z-index: 99;
    background-color: #fff;
    @media(max-width: 1198px) {
    top: 0;
}
`

const TopHeader = styled.div`
width: 100%;
height: 40px;
background-color: #F9F9F9;
position:fixed;
top: 0;
@media(max-width: 1198px) {
    display: none;
}
`


const ContainerIconTop = styled.div`
    max-width: 1780px;
    padding: 5px 60px;
    width: calc(100% - 120px);
    margin: 0 auto;
    box-sizing: border-box;
    display: flex;
    justify-content:center;
    align-items: center;
    transition: .3s linear;

    @media(max-width: 1198px){
        padding: 5px 10px;
        width: 100%;
        flex-direction: column;
        height: ${acfHeader => acfHeader.opened ? 'auto!important' : '0px!important'};
        opacity: ${acfHeader => acfHeader.opened ? '1' : '0'};
        pointer-events: ${acfHeader => acfHeader.opened ? 'all' : 'none'};
        align-items: self-start;
        justify-content: flex-start;


        a {
            margin-top: 15px;
            color: #6E7276;
        h4 {
            
                    font-weight: 700;
                    font-size: 16px;
                    color: #000000;
                    @media(max-width:900px) {
                        font-size: 14px;
                    }
        }
            img {
                height: 35px;
                box-shadow: 0 3px 6px #00000016;
                margin-right: 15px;
                padding: 7px;
                border-radius: 7px;
            }
            p {
                max-width: 480px;
                font-size: 14px;
                font-weight: 700;
                line-height: 26px;
                color: #6E7276;
            }
        }
        
    }
`


const FlexTextIconMobile = styled.div`
display: flex;
align-items: center;
margin-bottom: 15px;
`



const NameTopIcons = styled.div`
font-size: 14px;
display:flex;
align-items: center;
margin-right: 30px;
color: #a6a6a6;
@media(max-width:1198px) {
    margin-right: 0;
    background: #F9F9F9;
    margin: 15px -45px 0;
    padding: 7px 40px;
}
img {
    height: 25px;
    margin: 0 15px;
    filter: grayscale(1);
}
`
const FlexIconsproduct = styled.div`
display: flex;
align-items: center;
a {
    display: flex;
    align-items: center;
    margin-right: 30px;
    img {
        height: 25px;
        filter: grayscale(1);
        transition: .1s linear;
    }

}
`


const IconTop = styled.div`


`
const BtnMore = styled.div`
a {
    font-size: 14px;
    color: #a6a6a6;
    transition: .1s linear;
    border-bottom: 1px solid #a6a6a6;;
    &:hover {
        color: #377DFF;
        border-bottom: 1px solid #377DFF;
    }

}
`

// top icons mobile
const TopHeaderMobile = styled.div`
position: auto;
width: 100%;
height: 40px;
display: none;
@media(max-width: 1198px) {
    display: block;
}
`

const ArrowAnimate = styled.div`
width: 16px;
height: 16px;
padding: 0 10px;
display: flex;
align-items: center;
justify-content: center;
transform: ${acfHeader => acfHeader.opened ? 'rotate(180deg)' : 'rotate(0deg)'};
                transition: .2s linear;
    img {
        max-width:100%;
        height: auto;
        filter: invert(47%) sepia(5%) saturate(353%) hue-rotate(169deg) brightness(92%) contrast(91%);
        transition: .6s linear;
      
    }

`



const LocFlex = styled(Flex)`
    width: 100%;

`

const Container = styled.div`
    max-width: 1780px;
    padding: 16px 60px;
    width: calc(100% - 120px);
    margin: 0 auto;
    height: 100px;
    box-sizing: border-box;
    display:flex;
    align-items: center;

    @media(max-width: 1680px){
        padding: 16px 45px;
        width: calc(100% - 90px);
    }
    @media(max-width: 900px){
        padding: 30px 15px;
        width: calc(100% - 30px);
    }
`





const MobileButton = styled.div`
    display: none;
    @media(max-width: 1198px){
        display: block;
        padding: 10px 0;
        margin-left: 30px;
        width: 45px;

        &::before{
            content: '';
            position: absolute;
            border-radius: 4px;
            width: 45px;
            height: 4px;
            background-color: #3F4143;
            transform:  ${acfHeader => acfHeader.opened ? 'translateX(80px)' : ''};
            opacity: ${acfHeader => acfHeader.opened ? '0' : '1'};
            transition: .2s linear;
        }

        span{
            width: 45px;
            height: 4px;
            position: relative;
            display: block;

            &::before{
                content: '';
                position: absolute;
                border-radius: 4px;
                width: 45px;
                height: 4px;
                background-color: #3F4143;
                transform:  ${acfHeader => acfHeader.opened ? 'rotateZ(45deg)' : 'translateY(-10px)'};
                transition: .2s linear;
            }
            &::after{
                content: '';
                position: absolute;
                border-radius: 4px;
                width: 45px;
                height: 4px;
                background-color: #3F4143;
                transform: ${acfHeader => acfHeader.opened ? 'rotateZ(-45deg)' : 'translateY(10px)'};
                transition: .2s linear;
            }
        } 
    }
`

const Logo = styled(NavLink)`
    display: flex;
    align-items: center;
    img{
        @media(max-width: 1350px){
            max-width: 160px;
        }
    }
`

const Navigation = styled.nav`
    @media(max-width: 1198px){
        position: absolute;
        top: 100px;
        bottom: 0;
        background-color: #fff;
        height: calc(100vh - 150px);
        left: 0;
        right: 0;
        padding: 0 45px;
        transition: .2s;
        opacity: ${acfHeader => acfHeader.opened ? '1' : '0'};
        pointer-events: ${acfHeader => acfHeader.opened ? 'all' : 'none'};
        overflow-y: auto;
    overflow-x: hidden;
    }
    @media(max-width: 992px){
        height: calc(100vh - 107px);
    }
    @media(max-width: 900px){
        padding: 0;
    }
`

const List = styled.ul`
    padding-right: 15px;

    @media(max-width: 1350px){
        padding-right: 0;
    }

    @media(max-width: 1198px){
        display: flex;
        flex-direction: column;
    }
`

const ListItem = styled.li`
    display: ${acfHeader => acfHeader.flag ? 'none' : 'inline-block'};
    margin-right: 15px;
    position: relative;
    transition: all .3s;

    div{   
        transition: all .3s;
    }

    @media(max-width: 1198px){
        position: unset;
    }

    @media(max-width: 992px){
        display: inline-block;
        span, a{
            display: flex;
            align-items: center;
            position: unset;
            margin-left: 0;
            color: #000;
        }
        div, p{
            display: flex;
            margin-left: 0;
        }

    }
    @media(max-width: 900px){
        div, a{
            font-size: 14px;
        }
    }

    &:hover{
        div{
            @media(min-width: 1198px){
                box-shadow: 0 3px 6px 0 #00000016;
                cursor: default;
                position: relative;
                border-top-left-radius: 6px;
                border-top-right-radius: 6px;

                &::after{
                    transition: .0s;
                    width: 100%;
                    content: '';
                    background-color: #fff;
                    height: 4px;
                    bottom: 0;
                    position: absolute;
                    left: 0;
                    z-index: 101;
                }
            }
        }
        span, div, a{
            transition: .15s linear;
            color: #377DFF;

            &::before, &::after{
                color: #377DFF;
            }
        }
        ul{
            opacity: 1;
            pointer-events: all;

            a{
                color: #000;
                &:hover{
                    color: #377DFF;
                }
            }

            @media(max-width: 1198px){ 
                transform: translateX(0);
                opacity: 1;
            }
        }
    }
`

const ListLink = styled(NavLink)`
    display: block;
    padding: 10px 15px;
    color: #000000;
    font-weight: bold;

    img{
        width: 12px;
        height: 10px;
        margin-left: 3px;
    }

    @media(max-width: 1640px){
        font-size: 14px;
        padding: 10px 8px;
    }
`

const ListDiv = styled.div`
    display: block;
    padding: 10px 15px;
    color: #000000;
    font-weight: bold;

    img{
        width: 12px;
        height: 10px;
        margin-left: 3px;
    }

    @media(max-width: 1640px){
        font-size: 14px;
        padding: 10px 8px;
    }
`

const InnerList = styled.ul`
    position: absolute;
    top: 50px;
    left: 0px;
    opacity: 0;
    transition: .3s;
    transition: opacity .3s;
    box-shadow: 0 3px 6px 0 #00000016;
    box-sizing: border-box;
    padding: 30px;
    background-color: #fff;
    z-index: 100;
    pointer-events: none;

    @media(max-width: 1198px){
        box-shadow: 1px 6px 6px 0 #00000016;
        opacity: 1;
        top: 0;
        left: 20%;
        right: 0;
        bottom: 0;
        opacity: 0;
        transform: translateX(100%);

        @media(max-width: 992px){
            left: 260px;
        }

        @media(max-width: 900px){
            left: 200px;
            font-size: 14px;
        }

        @media(max-width: 768px){
            left: 160px;
        }

        &::before{
            display: none;
        }
    }

    li{
        min-width: 200px;
        @media(max-width: 992px){
            min-width: auto;
        }

    }
`

const FirstButtonInner = styled(Link)`
    padding: 20px 60px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000000;

    &:hover {
        background-color: #377DFF!important;
        color: #FFFFFF;
    }

    @media(max-width: 1760px){ 
        padding: 10px 20px;
        font-size: 16px;
    }

    @media(max-width: 640px){
        display: none;
    }
`

const FirstButtonOuter = styled.a`
    padding: 12px 60px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000000;
    transition: all.2s linear;
    
    &:hover {
        background-color: #377DFF!important;
        color: #FFFFFF;
    }

    @media(max-width: 1760px){ 
        padding: 10px 20px;
        font-size: 16px;
    }

    @media(max-width: 640px){
        display: none;
    }
`

const SecondButtonInner = styled(Link)`
    padding: 12px 60px;
    border: 2px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;

    &:hover {
        border-color: #377DFF;
        background-color: #FFFFFF;
        color: #377DFF;
    }

    @media(max-width: 1760px){ 
        padding: 10px 20px;
        font-size: 16px;
    }

    @media(max-width: 640px){
        display: none;
    }
`

const SecondButtonOuter = styled.a`
    padding: 12px 60px;
    border: 2px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    transition: all.2s linear;

    &:hover {
        border-color: #377DFF;
        background-color: #FFFFFF;
        color: #377DFF;
    }

    @media(max-width: 1760px){ 
        padding: 10px 20px;
        font-size: 16px;
    }

    @media(max-width: 640px){
        display: none;
    }
`

const FlagChoose = styled.div`
    margin-left: 30px;
    display: flex;
    position: relative;

    &:hover{
        div{
            opacity: 1;
            pointer-events: all;
        }
    }
    @media(max-width: 992px){
        display: none;
    position: unset;
    }
`

const FlagLink = styled.a`
    margin-left: 30px;
    display: flex;
    position: relative;

    &:hover{
        div{
            opacity: 1;
            pointer-events: all;
        }
    }
    @media(max-width: 992px){
        display: none;
    position: unset;
    }
`

const FlagName = styled.p`
    padding: 0 15px;
    font-weight: bold;
    color: #000;
    @media(max-width: 1184px){
        display: none;
    }
`

const FlagButton = styled.button`
    border: none;
    background-color: transparent;
    img{
        width: 12px;
        height: 10px;
        margin-left: -10px;
    }
    @media(max-width: 1184px){
        display: none;
    }
`

const AnotherFlags = styled.div`
    transition: .3s linear;
    position: absolute;
    top: 30px;
    left: -30px;
    padding: 10px 0;
    opacity: 0;
    pointer-events: none;
`

const FlagImg = styled.img`
    width: unset!important;
    height: unset!important;
`


const Header = ({ state }) => {

    const Header = state.source.get("/header/")
    const acfHeader = state.source[Header.type][Header.id]['acf']

    useGaTracker(acfHeader.id)

    const [isScriptAdded, changeIsScriptAdded] = useState(false)

    useEffect(() => {
        if (!isScriptAdded && acfHeader.scripts.repeater) {
            acfHeader.scripts.repeater.forEach(el => {
                const script = document.createElement("script")

                script.src = el.script_src
                script.async = true;

                document.body.appendChild(script)

                changeIsScriptAdded(true)
            })
        }
    }, [acfHeader])

    const [isMobileMenuOpened, isMobileMenuOpenedChange] = useState(false)
    const [isMobileMenuOpenedProducts, isMobileMenuOpenedChangeProducts] = useState(false)

    const queryForm = function (settings) {
        var reset = settings && settings.reset ? settings.reset : true;
        var self = window.location.toString();
        var querystring = self.split("?");
        if (querystring.length > 1) {
            var pairs = querystring[1].split("&");
            for (i in pairs) {
                var keyval = pairs[i].split("=");
                if (reset || sessionStorage.getItem(keyval[0]) === null) {
                    sessionStorage.setItem(keyval[0], decodeURIComponent(keyval[1]));
                }
            }
        }
        var hiddenFields = document.querySelectorAll("input[type=hidden], input[type=text]");
        console.log(hiddenFields)
        for (var i = 0; i < hiddenFields.length; i++) {
            var param = sessionStorage.getItem(hiddenFields[i].name);
            if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
        }
    }

    function goToRegister(action) {

        queryForm();
        var form = document.getElementById('goToRegisterForm');
        form.action = action;
        form.submit();

    }


    return (
        <HeaderContainer>
            <TopHeader>
                <IconTop>
                    <ContainerIconTop>
                        <NameTopIcons>
                            Explore other <img alt='Arrows' src={Arrows} /> products ˃˃
                        </NameTopIcons>
                        <FlexIconsproduct>
                            <a href="https://helpwise.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank"><img alt='Helpwise' src={Helpwise} /></a>
                            <a href="https://justcall.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank"><img alt='JustCall' src={JustCall} /></a>
                            <a href="https://iq.justcall.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank"><img alt='JustCallIQ' src={JustCallIQ} /></a>
                        </FlexIconsproduct>
                        <BtnMore>
                            <a href="https://www.saaslabs.co/" target="_blank">More</a>
                        </BtnMore>
                    </ContainerIconTop>
                </IconTop>
            </TopHeader>



            <Container>
                <LocFlex>
                    <div>
                        <Logo onClick={() => { isMobileMenuOpenedChange(false) }} link='/'><img alt={acfHeader.logo_alt} src={acfHeader.logo} /></Logo>
                    </div>
                    <Flex>
                        <Navigation opened={isMobileMenuOpened} >
                            <List>
                                {
                                    acfHeader.navigation.repeater.map((el, index) =>
                                        <ListItem key={index}>
                                            {
                                                el.inner_links
                                                    ? <ListDiv>
                                                        {el.text}
                                                        <span>
                                                            <img alt='arrow' src={Arrow} />
                                                        </span>
                                                        <InnerList>
                                                            {
                                                                el.inner_links.map((innerEl, innerIndex) =>
                                                                    <li key={innerIndex}>
                                                                        {
                                                                            innerEl.link_type
                                                                                ? <ListLink onClick={() => { isMobileMenuOpenedChange(false) }} link={innerEl.link}>
                                                                                    {innerEl.text}
                                                                                </ListLink>
                                                                                : <ListLink onClick={() => { isMobileMenuOpenedChange(false) }} target='_blank' href={innerEl.link}>
                                                                                    {innerEl.text}
                                                                                </ListLink>
                                                                        }

                                                                    </li>
                                                                )
                                                            }
                                                        </InnerList>
                                                    </ListDiv>
                                                    : <ListLink onClick={() => { isMobileMenuOpenedChange(false) }} link={el.link}>
                                                        {el.text}
                                                    </ListLink>
                                            }

                                        </ListItem>
                                    )
                                }
                                <ListItem flag={true}>
                                    <ListDiv>
                                        {
                                            acfHeader.form_free_test.current_language.map((el, index) =>
                                                <FlagChoose key={index} >
                                                    <FlagImg alt={el.flag_alt} src={el.flag} />
                                                    <FlagName>{el.title}</FlagName>
                                                    <InnerList>
                                                        {
                                                            acfHeader.form_free_test.repeater.map((innerEl, innerIndex) =>
                                                                <FlagLink key={innerIndex} rel="noreferrer" target='_blank' href={innerEl.link}>
                                                                    <FlagImg alt={innerEl.icon_alt} src={innerEl.flag} />
                                                                    <FlagName>{innerEl.title}</FlagName>
                                                                </FlagLink>
                                                            )
                                                        }
                                                    </InnerList>
                                                </FlagChoose>
                                            )
                                        }
                                    </ListDiv>
                                </ListItem>

                            </List>


                            <TopHeaderMobile>
                                <NameTopIcons opened={isMobileMenuOpenedProducts} onClick={() => { isMobileMenuOpenedChangeProducts(!isMobileMenuOpenedProducts) }} >
                                    <img alt='Arrows' src={Arrows} /> Explore platform
                                    <ArrowAnimate opened={isMobileMenuOpenedProducts} ><img src={Arrow} /></ArrowAnimate>



                                </NameTopIcons>
                                <ContainerIconTop opened={isMobileMenuOpenedProducts} >




                                    <a href="https://helpwise.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank">
                                        <FlexTextIconMobile><img alt='JustCallIQMobile' src={HelpWiseMobile} /><h4>Helpwise</h4></FlexTextIconMobile>
                                        <p>A shared inbox software</p>

                                    </a>
                                    <a href="https://justcall.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank">
                                        <FlexTextIconMobile><img alt='JustCallMobile' src={JustCallMobile} /><h4>Just Call</h4></FlexTextIconMobile>
                                        <p>A 24/7 cloud phone system</p>

                                    </a>
                                    <a href="https://iq.justcall.io/?utm_source=callpage&utm_medium=website&utm_campaign=saaslabsplatform" target="_blank">
                                        <FlexTextIconMobile><img alt='iqMobile' src={JustCallIQMobile} /><h4>Just Call IQ</h4></FlexTextIconMobile>
                                        <p>‘Super brain’ for your reps</p>

                                    </a>


                                </ContainerIconTop>
                            </TopHeaderMobile>




















                        </Navigation>
                        <Flex>
                            {
                                acfHeader.form_free_test.first_button_type
                                    ? <FirstButtonInner link={acfHeader.form_free_test.first_button_link}>
                                        {acfHeader.form_free_test.first_button_text}
                                    </FirstButtonInner>
                                    : <FirstButtonOuter rel="noreferrer" target='_blank' href={acfHeader.form_free_test.first_button_link}>
                                        {acfHeader.form_free_test.first_button_text}
                                    </FirstButtonOuter>
                            }
                            {
                                acfHeader.form_free_test.second_button_type
                                    ? <SecondButtonInner link={acfHeader.form_free_test.second_button_link}>
                                        {acfHeader.form_free_test.second_button_text}
                                    </SecondButtonInner>
                                    : <SecondButtonOuter rel="noreferrer" target='_blank' onClick={() => { goToRegister(acfHeader.form_free_test.second_button_link) }} >
                                        {acfHeader.form_free_test.second_button_text}
                                    </SecondButtonOuter>
                            }
                            {
                                acfHeader.form_free_test.current_language.map((el, index) =>
                                    <FlagChoose key={index}>
                                        <img alt={el.flag_alt} src={el.flag} />
                                        <FlagName>{el.title}</FlagName>
                                        <FlagButton><img alt='arrow' src={Arrow} /></FlagButton>
                                        <AnotherFlags>
                                            {
                                                acfHeader.form_free_test.repeater.map((innerEl, innerIndex) =>
                                                    <FlagLink key={innerIndex} rel="noreferrer" target='_blank' href={innerEl.link}>
                                                        <img alt={innerEl.flag_alt} src={innerEl.flag} />
                                                        <FlagName>{innerEl.title}</FlagName>
                                                    </FlagLink>
                                                )
                                            }
                                        </AnotherFlags>
                                    </FlagChoose>
                                )
                            }


                        </Flex>
                        <MobileButton opened={isMobileMenuOpened} onClick={() => { isMobileMenuOpenedChange(!isMobileMenuOpened) }} ><span /></MobileButton>
                    </Flex>


                </LocFlex>

            </Container>
        </HeaderContainer >
    )
}

export default connect(Header)