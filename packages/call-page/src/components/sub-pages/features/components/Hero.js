import React from 'react'
import { styled } from "frontity"
import { Container, Text } from '../../../../../common/styles'

const Article = styled.article`
    padding-top: 90px;
    position: relative;
    z-index: 0;
`

const Flex = styled.div`
    justify-content: space-evenly;
    display: flex;
    align-items: center;
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 30px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 565px){
        font-size: 28px;
        line-height: 40px;
    }
`

const TextWrapper = styled.div`
    max-width: 840px;
    text-align: center;
    @media(max-width: 950px){
        text-align: left;
    }
`

const LocText = styled(Text)`
    font-size: 28px;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 28px;
    }
    @media(max-width: 950px){
        max-width: 500px;
    }
    @media(max-width: 565px){
        font-size: 16px;
        line-height: 24px;
    }
`

const Hero = (props) => {
    return (
        <Article>
            <Container>
                <Flex>
                    <TextWrapper>
                        <Title>{props.acf.title}</Title>
                        <LocText>{props.acf.text}</LocText>
                    </TextWrapper>
                </Flex>
            </Container>
        </Article>
    )
}

export default Hero