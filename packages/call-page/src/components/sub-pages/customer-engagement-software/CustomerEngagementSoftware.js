import React from 'react'
import { connect } from "frontity"
import SEO from '../../../../common/SEO/seo'
import Hero from './components/Hero'
import About from './components/About'
import Awards from '../../../../common/bricks/Awards'



const CustomerEngagementSoftware = ({ state }) => {

    const Main2 = state.source.get("/customer-engagement-software/")
    const acfMain2 = state.source[Main2.type][Main2.id]['acf']



    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <main className='body'>
            {data.isReady
                ? <>
                    <SEO seo={props.acf.seo} date={props.date_gmt} />
                    <Hero acf={props.acf.hero} form_free_test={props.acf.form_free_test} />
                    <About acf={acfMain2.third_about} />
                    <Awards acf={props.acf.awards} />
                </>
                : null
            }
        </main>
    )
}

export default connect(CustomerEngagementSoftware)