import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../../../common/styles'
import BlogPartItem from './BlogPartItem'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(90px, 11vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '90px;'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`


const Grid = styled.div`
    display: grid;
    justify-content: space-between;
    grid-template-columns: 387px 387px 387px 387px;
    margin: 0 0 120px;
    @media(max-width: 1760px){
        grid-template-columns: 587px 587px;
        justify-content: space-evenly;
    }
    @media(max-width: 1198px){
        grid-template-columns: 387px 387px;
    }
    @media(max-width: 1000px){
        grid-template-columns: 1fr;
    }
`


const BlogPart = (props) => {
    return (
        <Article id='blog'>
            <Container>
                <Grid>
                    {
                        props.caseStudies[0]
                            ? <>
                                {
                                    props.caseStudies.map((el, index) =>
                                        <BlogPartItem key={index} el={el} state={props.state} />
                                    )
                                }
                            </>
                            : null
                    }
                </Grid>
            </Container>
        </Article >
    )
}

export default BlogPart