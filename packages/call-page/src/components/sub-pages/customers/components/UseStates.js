import React from 'react'
import { styled } from "frontity"
import NavLink from "@frontity/components/link"

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Container = styled.div`
    width: calc(100% - 120px);
    max-width: 1640px;
    margin: 0 auto;
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

const Title = styled.h2`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    text-align: center;
    max-width: 1080px;
    margin: 0 auto;

    @media (max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        max-width: 1080px;
        padding: 0 0 55px;
    }

    @media (max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding: 0 0 55px;
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
    max-width: 1680px;
    margin: 0 auto;
`

const Item = styled.div`
    width: 7%;
    text-align: center;
    margin-top: 60px;
    color: #000;
    transition: .2s linear;
    cursor: pointer;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-end;
    box-shadow: 0 3px 6px #00000016;
    padding: 15px;

    &:hover{
        p{
            color: #377DFF;
        }
        transform: translateY(-4px) translateX(2px);
    }

    img{
        margin: 15px 0;
        max-width: 60px;


        @media(max-width: 1350px){
            margin-top: 45px;
            margin-bottom: 30px;
        }

        @media(max-width: 1198px) {
            max-width: 60px;
        }

        @media(max-width: 992px){
            max-width: 60px;
        }
    }

    @media(max-width: 1350px){
        width: 15%;
        padding-top: 10px;
    }

    @media(max-width: 1198px){
        width: 20%;
    }

    @media(max-width: 992px){
        font-size: 16px;
        line-height: 24px;
    }

    @media(max-width: 899px){
        padding-top: 0;
        width: 25%;
    }

    @media(max-width: 650px){
        width: 33%;
    }

    @media(max-width: 496px){
        width: 100%;
        box-sizing: border-box;
    }

    p {
        font-size: 14px;
        font-weight: bold;
        color: #000;

        @media(max-width:1198px) {
            font-size: 18px;
        }

        @media(max-width:764px) {
            font-size: 16px;
        }
    }
`

const UseStates = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <Flex>
                    {props.acf.repeater.map((el, index) =>
                        <Item key={index}>
                            <NavLink link={`/category_casestudies/${el.type}`}>
                                <img alt={el.icon_alt} src={el.icon} />
                                <p>{el.text}</p>
                            </NavLink>
                        </Item>
                    )}
                </Flex>
            </Container>
        </Article>
    )
}

export default UseStates