import React from 'react'
import Clock from '../../../../common/sprites/clock.png'
import NavLink from "@frontity/components/link"
import { styled } from "frontity"
import { Flex } from '../../../../common/styles'


const RelatedPosts = styled.div`
    max-width: 526px;
    border-radius: 12px;
    box-shadow: 0 3px 6px 0 #00000016;
    width: 100%;
    position: relative;

    a{
        color: #000000;
    }

    img{
        width: 100%;
        border-top-right-radius: 12px;
        border-top-left-radius: 12px;
    }

    h3{
        padding: 0 15px 120px;
    }

    @media(max-width: 1198px){
        margin: 0 auto 30px;
    }
`

const InfoFlex = styled(Flex)`
    padding: 0 15px;
    position: absolute;
    bottom: 30px;
    width: calc(100% - 60px);
    color: #6E7276;
`

const TypesFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 15px 10px;
`

const TypesItem = styled.p`
padding: 7px 15px;
border: 2px solid #377DFF;
border-radius: 3px;
font-size: 14px;
line-height: 21px;
font-weight: bold;
margin: 0 15px 15px 0;
`

const RelatedPostItem = (props) => {
    const data = props.state.source[props.el.type][props.el.id]
    let currentCategories = []


    if (data.category_blog) {
        data.category_blog.forEach(el => {
            props.allCategories.items.forEach(innerEl => {
                if (innerEl.id == el) {
                    currentCategories.push(innerEl)
                }
            })
        })
    }
    return (
        <RelatedPosts>
            <NavLink link={'blog/' + data.slug}>
                <img alt={data.acf.preview.img_alt} src={data.acf.preview.img} />
            </NavLink>
            <TypesFlex>
                {currentCategories
                    ? currentCategories.map((el, index) =>
                        <NavLink link={'category_blog/' + el.slug}>
                            <TypesItem key={index}>
                                {el.name}
                            </TypesItem>
                        </NavLink>
                    )
                    : null
                }
            </TypesFlex>
            <NavLink link={'blog/' + data.slug}>
                <h3>{data.acf.preview.title}</h3>
                <InfoFlex>
                    <div style={{ display: 'flex', 'align-items': 'center' }}>
                        <img style={{ width: '48px', height: '48px', 'margin-right': '10px' }} alt='post author' src={data.acf.preview.author_img} />
                        <div>
                            <p style={{ 'line-height': '24px' }}>Author</p>
                            <p style={{ 'line-height': '24px' }}>{data.acf.preview.author}</p>
                        </div>
                    </div>
                    <div style={{ display: 'flex', 'align-items': 'center' }}>
                        <img style={{ 'width': '24px', 'margin-right': '5px' }} alt='clock' src={Clock} /><span>{data.acf.preview.time}{'m'}</span>
                    </div>
                </InfoFlex>
            </NavLink>
        </RelatedPosts>
    )
}

export default RelatedPostItem