import React, { useState, useEffect } from 'react'
import { Container, Flex } from '../../../../common/styles'
import { styled } from "frontity"
import ContentItem from './ContentItem'
import NavLink from "@frontity/components/link"

/* eslint-disable */

const Article = styled.article`
    padding-top: 120px;
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 6fr;
    grid-column-gap: 30px;
`

const LocFlex = styled(Flex)`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-row-gap: 30px;
    grid-column-gap: 30px;

    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 764px){
        grid-template-columns: 1fr;
    }
`


const ButtonPagination = styled(NavLink)`
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};
    font-size: 22px;

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const ButtonPaginationDisabled = styled.button`
    font-size: 22px;
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const Pagination = styled.div`
    padding: ${props => props.bottom ? '30px 0 0 0' : '0 0 30px 0'};
    display: flex;
    align-items: center;
    justify-content: center;
`

const Content = (props) => {
    let [arrays, changeArrays] = useState([])

    useEffect(() => {
        let arr = []
        for (let i = 1; i <= props.posts.length; i++) {
            arr.push(props.posts[i - 1])
        }
        changeArrays(arr)
    }, [props.state.router.link])


    return (
        <Article>
            <Container>
                <Pagination>
                    {
                        props.postsData.previous
                            ? <ButtonPagination link={props.postsData.previous}>{'<'}</ButtonPagination>
                            : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                    }
                    <ButtonPaginationDisabled page>{props.postsData.page}</ButtonPaginationDisabled>
                    {
                        props.postsData.next
                            ? <ButtonPagination link={props.postsData.next}>{'>'}</ButtonPagination>
                            : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                    }
                </Pagination>
                <LocFlex>
                    {arrays.map((el, index) =>
                        <ContentItem key={index} el={el} state={props.state} />
                    )}
                </LocFlex>
                <Pagination bottom>
                    {
                        props.postsData.previous
                            ? <ButtonPagination link={props.postsData.previous}>{'<'}</ButtonPagination>
                            : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                    }
                    <ButtonPaginationDisabled page>{props.postsData.page}</ButtonPaginationDisabled>
                    {
                        props.postsData.next
                            ? <ButtonPagination link={props.postsData.next}>{'>'}</ButtonPagination>
                            : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                    }
                </Pagination>
            </Container>
        </Article>
    )
}

export default Content