import React from 'react'
import { styled } from "frontity"
import { Container, Link } from '../styles'
import Background from '../sprites/concrete_bg.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin: 0 auto 180px;
    text-align: center;
    max-width: 1080px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin:0 auto 110px;
        max-width: 510px;
    }
    @media(max-width: 768px){
        font-size: 28px;
        line-height: 40px;
        margin:0 auto 80px;
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 350px;
    bottom: 0;
    left: 0;
    z-index: 0;
    @media(max-width: 1752px){
        display: none;
    }
`

const ListItem = styled.li`
    max-width: 387px;
    background-color: #fff;
    position: relative;
    bottom: 0px;
    position: relative;
    z-index: 10;
    border-radius: 3px;
    box-shadow: 0px 3px 6px 0 #00000016;
    @media(max-width: 1752px){
        margin: 0 auto;
    }

    @media(max-width: 1440px){
        max-width: 100%;
    }

    @media(max-width: 1028px){
        max-width: 665px;
    }

    @media(max-width: 764px){
        max-width: 100%;
    }
`

const List = styled.ul`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 60px;
    justify-content: space-between;

    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
    }

    @media(max-width: 1028px){
        grid-template-columns: 1fr;
    }
`

const ListLink = styled(Link)`
    display: block;
    padding: 20px 35px;
    width: 180px;
    position: absolute;
    bottom: 60px;
    color: #000000;
    background-color: transparent;
    border: 2px solid #377DFF;
    box-shadow: 0px 3px 6px 0 #D7E5FF;
    transition: all.2s linear;
    cursor: pointer;
    text-align: center;

&:hover {
    border-color: #377DFF;
    background-color: #377DFF;
    color: #FFFFFF;
}

    @media(max-width: 1028px){
        left: 50%;
        transform: translateX(-50%);
        padding: 10px 35px;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 28px;
        padding: 10px 20px;
    }

`

const ListText = styled.p`
    padding-bottom: 120px;
    color: #6E7276;
    line-height: 28px;

    @media(max-width: 764px){
        font-size: 14px;
        line-height: 22px;
    }
`

const ListTitle = styled.h3`
    padding: 60px 0 40px;
    font-weight: bold;

    @media(max-width: 1028px){
        padding: 60px 0 20px;
    }

    @media(max-width: 764px){
        font-size: 16px;
        line-height: 24px;
    }
`

const ListContent = styled.div`
    padding: 30px;
    position: relative;
    height: 100%;
    box-sizing: border-box;
`

const Img = styled.img`
    position: absolute;
    border-radius: 6px;
    top: 0;
    transform: translate(-15px, -33%);
`

const Increase = (props) => {
    const array = Object.values(props.acf.increase_repeter)

    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <List>
                    {array.map((el, index) =>
                        <ListItem key={index}>
                            <ListContent>
                                <Img alt={el.img_alt} src={el.img1} />
                                <ListTitle>{el.title}</ListTitle>
                                <ListText>{el.text}</ListText>
                                {el.button_type
                                    ? <ListLink link={el.button_link}>{el.button_text}</ListLink>
                                    : <ListLink as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</ListLink>
                                }
                            </ListContent>
                        </ListItem>
                    )}
                </List>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    );
}

export default Increase;