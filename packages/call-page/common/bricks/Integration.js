import React from 'react'
import { Container, Flex, Link } from '../styles'
import { styled } from "frontity"

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    padding: 0 0 80px;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding: 0 0 40px;
    }
`

const TextTitle = styled.h3`
    font-size: 22px;
    text-align: center;
    @media(max-width: 876px){
        display: block;
    }
`

const Text = styled.p`
    padding: 10px 0 55px;
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 22px;
        padding: 10px 0 45px;
    }
`

const TextWrapper = styled.div`
    max-width: 665px;
    display: flex;
    flex-direction: column;
    align-items: center;
`

const ImgWrapper = styled.div`
    display: none;

    @media(max-width: 1198px){
        display: block;
    }

    img{
        max-width: 740px;
        width: 100%;
        margin: 0 0 60px;

        @media(max-width: 480px){
            margin: 0 0 40px;
        }
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    align-items: center;

    @media(max-width: 1198px) {
        grid-template-columns: 1fr;
    }
`

const LocContainer = styled(Container)`
    background-size: cover;
    background-repeat: no-repeat;
    padding: 0;
`

const LocFlex = styled(Flex)`
    justify-content: center;
    flex-direction: column;
`

const ImgLeft = styled.div`
    img {
        max-width: 100%;
        height: auto;
    }

    @media (max-width:1198px) {
        display: none;
    }
`

const ImgRight = styled.div`
    img {
        max-width: 100%;
        height: auto;
    }
    @media (max-width:1198px) {
        display: none;
    }
`

const ContainerCustom = styled.div`
    max-width:100%;
    margin: 0 auto;
    padding: 0 100px;

    @media (max-width:1198px) {
        padding: 0;
    }
`

const LocLink = styled(Link)`
    transition: all.2s linear;
    text-align: center;

    @media(max-width:767px) {
        padding: 20px 10px 
    }

    &:hover {
        border-color: #377DFF;
        background-color: #FFFFFF;
        color: #000000;
    }
`


const Integration = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <ContainerCustom>
                <Grid>
                    <ImgLeft><img alt={props.acf.img_left_alt} src={props.acf.img_left} /></ImgLeft>
                    <LocContainer img={props.acf.img}>
                        <Title>{props.acf.title}</Title>
                        <LocFlex>
                            <ImgWrapper>
                                <img alt={props.acf.img_alt} src={props.acf.img} />
                            </ImgWrapper>

                            <TextWrapper>
                                <TextTitle>{props.acf.text_title}</TextTitle>
                                <Text>{props.acf.text}</Text>
                                {
                                    props.acf.button_type
                                        ? <LocLink link={props.acf.button_link}>{props.acf.button_text}</LocLink>
                                        : <LocLink as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</LocLink>
                                }
                            </TextWrapper>

                        </LocFlex>
                    </LocContainer>
                    <ImgRight><img alt={props.acf.img_right_alt} src={props.acf.img_right} /></ImgRight>
                </Grid>
            </ContainerCustom>

        </Article>
    )
}

export default Integration