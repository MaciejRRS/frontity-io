import React from 'react'
import { styled } from "frontity"
import { Container, Text } from '../styles'

/* eslint-disable */

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    @media(max-width: 1198px){
        grid-template-columns: 1fr;
    }
`

const Section = styled.section`
    @media(max-width: 1198px){
        margin-top: -20px;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 120px;
    @media(max-width: 1198px){
        padding: 0 0 90px;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        padding: 0 0 60px;
        font-size: 28px;
        line-height: 40px;
    }
`

const Summary = styled.summary`
    padding-left: 45px;
    position: relative;
    cursor: pointer;
    margin: 40px 45px 20px 0;
    span{
        font-size: 28px;
        font-weight: bold;
        line-height: 40px;
    }
    &::before{
        content: "+";
        position: absolute;
        left: 0;
        color: #377DFF;
        font-size: 28px;
        line-height: 40px;
        font-weight: bold;
    }
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 24px;
    }
`
const LocText = styled(Text)`
    padding: 0 45px;
    span{   
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 24px;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 22px;
    }
`
const Questions = (props) => {
    let arrays = [[], []]
    for (let i = 1; i <= props.acf.repeater.length; i++) {
        if (i % 2 == 0) {
            arrays[1].push(props.acf.repeater[i - 1])
        } else {
            arrays[0].push(props.acf.repeater[i - 1])
        }
    }
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container >
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        arrays.map(arr =>
                            <Section>
                                {
                                    arr.map((el, index) =>
                                        <div key={index}>
                                            <details>
                                                <Summary itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                                                    <span itemprop="name">
                                                        {el.question}
                                                    </span>
                                                </Summary>
                                                <LocText itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
                                                    <span itemprop="text">
                                                        {el.answer}
                                                    </span>
                                                </LocText>
                                            </details>
                                        </div>
                                    )
                                }
                            </Section>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}
export default Questions