import React from 'react'
import HubspotForm from 'react-hubspot-form'


const ContactForm = (props) => {
    return (
        <>
            <HubspotForm
                portalId={props.portalId}
                formId={props.formId}
            />
        </>
    )
}

export default ContactForm